#!/bin/sh
# Percentatge límit abans d'avisar
LIMIT="85"

# A qui enviem el mail d'avis? Si MAILTO="" s'envia a la sortida estàndar
MAILTO="monica@sindicat.net"

# Subject del mail a enviar
SUBJECT="Alerta server USTEC: Espai de disc sobrepassat"

# Regexp de linies de "df" que no tindrem en compte
EXCLUDE="NONE"

# Programa

data=`LANG=C df -Ph | grep -v Filesystem | grep -v "$EXCLUDE" | tr "%" " " | awk '{print $1" "$6" "$5" "$4}'`

comprova()
{
        [ "$3" -ge "$LIMIT" ] && echo "La partició $1 assignada al directori $2 sobrepassa el limit del $LIMIT%: L'ús actual és del $3%, es disposa de $4."
        if [ "$#" -ge 5 ]; then
                shift 4
                comprova $@
                exit 0
        fi
}

result=`comprova $data`

if [ "$MAILTO" = "" ]; then
        [ "$result" != "" ] && echo "$result"
else
        [ "$result" != "" ] && echo "$result" | mailx -s "$SUBJECT" "$MAILTO"
fi
exit 0
