#!/bin/bash

# Copyright (C) 2018 Mònica Ramírez Arceda <monica@probeta.net>

# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free 
# Software Foundation, either version 3 of the License, or (at your option) 
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
# more details.
# 
# You should have received a copy of the GNU General Public License along with 
# this program. If not, see http://www.gnu.org/licenses/.

help() {
    echo "Usage: $0 [-h HOSTNAME] [-d DEVICE] [-t PRESEED_TEMPLATE] ISO_FILE"
    echo "       Generates a new ISO file based on PRESEED_TEMPLATE."
    echo ""
    echo "    -h                    display this help and exit"
    echo "    -n HOSTNAME           hostname of the machine where iso will be installed."
    echo "                          By default, HOSTNAME is debian."
    echo "    -d DEVICE             name of the device where iso will be installed (sda, sdb...)."
    echo "                          By default, DISK is sda."
    echo "    -t PRESEED_TEMPLATE   preseed filename. {{HOSTNAME}} and {{DEVICE}} will be replaced"
    echo "                          by HOSTNAME and DEVICE respectively."
    echo "                          By default, PRESEED_TEMPLATE is preseed_template.cfg."
}

HOSTNAME=debian
DISK=sda
PRESEED_TEMPLATE=preseed_template.cfg

while getopts "hn:d:t:" o; do
    case "${o}" in
        h)
            help
            exit 1
            ;;
        n)
            HOSTNAME=${OPTARG}
            ;;
        d)
            DISK=${OPTARG}
            ;;
        t)
            PRESEED_TEMPLATE=${OPTARG}
            ;;
        *)
            help
            exit 1
            ;;
    esac
done

shift $((OPTIND-1))
ISO=$1

# Prepare preseed file
echo -e "\nPreparing preseed file..."
sed "s/{{HOSTNAME}}/${HOSTNAME}/" preseed_template.cfg > preseed.cfg
sed -i "s/{{DEVICE}}/${DISK}/" preseed.cfg

# Create copy of image
echo -e "\nCopying image..."
mkdir isofiles
xorriso -osirrox on -indev $ISO -extract / isofiles

# Hack the initrd
echo -e "\nHacking initrd..."
chmod +w -R isofiles/install.amd/
gunzip isofiles/install.amd/initrd.gz
echo preseed.cfg | cpio -o -H newc -A -F isofiles/install.amd/initrd
gzip isofiles/install.amd/initrd
chmod -w -R isofiles/install.amd/

# Fix md5sum's
echo -e "\nFixing md5sums..."
cd isofiles
chmod +w md5sum.txt
md5sum `find -follow -type f` > md5sum.txt
chmod -w md5sum.txt
cd ..

# Create new image
echo -e "\nCreating new images..."
xorriso -as mkisofs -o ${HOSTNAME}.iso \
        -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin \
        -c isolinux/boot.cat -b isolinux/isolinux.bin -no-emul-boot \
        -boot-load-size 4 -boot-info-table isofiles

echo "Removing temp files..."
rm -rf isofiles

