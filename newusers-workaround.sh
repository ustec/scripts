#!/bin/bash
#
# Copyright (c) 2016 Mònica Ramírez Arceda <monica@probeta.net>
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# A workaround to run newusers for Debian bug. It is only valid for small 
# files because it become very slow.

# newusers works adding he last user in the file if the previous ones 
# already exist

if [ $# -ne 1 ]; then
    echo "Usage: newusers-workaround.sh passwd-file" 
    exit 1
fi

usersfile=$1
tmpfile=`mktemp /tmp/users-temp-XXX.csv`
# Calculate the number of lines of the incoming passwd file
nusers=`wc -l $usersfile | cut -f1 -d' '`

# For each user build a temp file with all the previous users and the 
# new one. In each iteration new users will add the las user.
for i in `seq 1 $nusers`; do
    echo "Adding user $i"
	head -$i $usersfile > $tmpfile
	newusers $tmpfile
done

rm $tmpfile
