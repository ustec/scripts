#!/usr/bin/env python3
#
# Copyright (c) 2016 Mònica Ramírez Arceda <monica@probeta.net>
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import argparse
import csv
import random
import string
import sys

def new_users_add_password(users_file, users_file_pw):
	# Open input passwd file, with empty password field
	with open(users_file, 'r') as inputfile:
		users = csv.reader(inputfile, delimiter=':')
		# Open output passwd file, where we will write input file content 
		# filling password field
		with open(users_file_pw, 'w') as outputfile:
			userspw = csv.writer(outputfile, delimiter=':',
			                     lineterminator='\n')
			# For each user
			for row in users:
				password = ''
				# Generate a password of 8 characters
				for _ in range(8):
					random_character = random.choice(string.ascii_letters + 
													 string.digits)
					password += random_character
			    # Write the user with her password
				userspw.writerow(
					[
						row[0], password, row[2], row[3], 
						row[4], row[5], row[6]
					]
				)

if __name__ == "__main__":
	parser =  argparse.ArgumentParser(description = 'Add random passwors to a passwd file')
	parser.add_argument('input_file', help = 'Input passwd filename')
	parser.add_argument('output_file', help = 'Output passwd filename')
	args = parser.parse_args()
	users_file = args.input_file
	users_file_pw = args.output_file
	new_users_add_password(users_file, users_file_pw)
