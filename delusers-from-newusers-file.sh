#!/bin/bash
#
# Copyright (c) 2016 Mònica Ramírez Arceda <monica@probeta.net>
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Get the user names from a file with /etc/passwd format and 
# delete them from the system

if [ $# -ne 1 ]; then
    echo "Usage: delusers-from-newusers-file.sh passwd-file" 
    exit 1
fi

while read -r line; do 
    deluser --remove-home `echo $line | cut -d: -f1` 
done < $1
